/*
 * File:   main_ADC_PWM.c
 * Author: Brandy
 *
 * Created on September 7, 2016, 7:53 PM
 */


#include <xc.h>
#include "Alteri.h"

void main(void) {
    TRISA=0;
    TRISD=0;
    ADCON0=0x01;
    ADCON0bits.CHS=1;
    // pg. 265
    // bit 5-2 CHS3:CHS0: Analog Channel Select 
    // AN1 is selected in this case Located in RA1
    // bit 0 ADON: A/D On bit
    // 1 = A/D converter module is enabled
    ADCON1=0x0D;
    /* pg. 266
     * bit 3-0 PCFG3:PCFG0: A/D Port Configuration Control bits:
     * in this case AN12 - AN2 are now Digital and only AN1 and AN0 is analog
     */
    ADCON2=0x00;
    /* pg. 267
     * bit 7 ADFM: A/D Result Format Select bit
       1 = Right justified
     *              ADRESH                                   
     * | 0 | 0 | 0 | 0 | 0 | 0 |MSB|   |
     *              ADRESL
     * |   |   |   |   |   |   |   |LSB|      
       0 = Left justified
     *              ADRESH                                   
     * |MSB|   |   |   |   |   |   |   |
     *              ADRESL
     * |   |LSB| 0 | 0 | 0 | 0 | 0 | 0 |      
     */
    TRISC = 0; // set PORTC as output
    PORTC = 0; // clear PORTC
    /*
     * configure CCP module as 2929.69 Hz PWM output
     */
    PR2 = 0b11111111;
    //pg. 150 Period Register This is used to seta frequency
    T2CON = 0b00000111;
    //pg. 139 Timer2 is on and Prescaler is 16
    CCP1CON = 0b00111100;
    //pg. 145 DCxB1:DCxB0These bits are the two LSbs (bit 1 and bit 0) 
    //of the 10-bit PWM duty cycle. The eight MSbs of the duty
    //cycle are found in CCPR1L. Both are set as 1
    //CCPxM3:CCPxM0: CCPx Module Mode Select bits
    //11xx = PWM mode
    CCP2CON = 0b00111100;
    while(1){
        ADCON0bits.GO=1;
        while(ADCON0bits.GO){
            LATD=(ADRESL>>6);
            CCPR1L=ADRESH;
            CCP1CONbits.DC1B=(ADRESL>>6);
            // pg. 151 and 156
            // The PWM duty cycle is specified by writing to the
            // CCPR1L register and to the CCP1CON<5:4> bits.
            CCPR2L=ADRESH;
            CCP2CONbits.DC2B=(ADRESL>>6);
        }
    }
    return;
}